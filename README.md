# Deploy to Heroku with Bitbucket Pipelines

*This example is forked from https://bitbucket.org/rjst/heroku-deploy by Richard Stephens.*

This repository contains an example of deployment integration with Heroku using the [Platform API](https://devcenter.heroku.com/articles/build-and-release-using-the-api)

## Setting up your Heroku deployment

Simply copy heroku-deploy.sh to the root of your repository and invoke it as part of your pipeline with the following variables defined.

This script depends on two environment variables to be set in Bitbucket Pipelines

1. $HEROKU_API_KEY - https://devcenter.heroku.com/articles/platform-api-quickstart
2. $HEROKU_APP_NAME - Your app name in Heroku